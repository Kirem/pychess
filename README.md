# INSTALLATION
```bash
git clone https://bitbucket.org/Kirem/pychess.git
cd pychess
pip install -e . -r requirements.cfg
```

# RUN TESTS
Please run tests from root_path for run all unittest and linting test
`py.test` or `tox`

# TODO
- [ ] Castling
- [ ] Promotion
- [ ] Check end game
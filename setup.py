# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
from setuptools import setup


setup(name='pychess',
      version='0.1',
      author='Jaroslaw Grycz, Miroslaw Karpiewski',
      author_email='jaroslaw.grycz@gmail.com, mirek0922@gmail.com',
      scripts=['scripts/pychess'])

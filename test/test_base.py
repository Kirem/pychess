# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.exception import IncorrectColumn, IncorrectRow, IncorrectPosition, IncorrectIndex
from pychess.base import BaseOperations
from unittest import TestCase


class TestBaseOperationsNotationToIndex(TestCase):
    def setUp(self):
        self.board = BaseOperations()

    def test_translate_col_to_index_pass(self):
        assert 0 == self.board.col_to_index('a')
        assert 4 == self.board.col_to_index('e')
        assert 7 == self.board.col_to_index('h')

    def test_translate_col_to_index_fail(self):
        with self.assertRaises(IncorrectColumn):
            self.board.col_to_index('1')

        with self.assertRaises(IncorrectColumn):
            self.board.col_to_index('z')

    def test_translate_row_to_index_pass(self):
        assert 0 == self.board.row_to_index('8')
        assert 5 == self.board.row_to_index('3')
        assert 7 == self.board.row_to_index('1')

    def test_translate_row_to_index_fail(self):
        with self.assertRaises(IncorrectRow):
            self.board.row_to_index('0')

        with self.assertRaises(IncorrectRow):
            self.board.row_to_index('9')

        with self.assertRaises(IncorrectRow):
            self.board.row_to_index('z')

    def test_notation_to_index_pass(self):
        assert (0, 0) == self.board.notation_to_index('a8')
        assert (7, 0) == self.board.notation_to_index('h8')
        assert (0, 7) == self.board.notation_to_index('a1')
        assert (7, 7) == self.board.notation_to_index('h1')

    def test_notation_to_index_fail(self):
        with self.assertRaises(IncorrectPosition):
            self.board.notation_to_index('dupa')

        with self.assertRaises(IncorrectPosition):
            self.board.notation_to_index(['a', 7])


class TestBaseOperationsIndexToNotation(TestCase):
    def setUp(self):
        self.board = BaseOperations()

    def test_translate_index_to_col_pass(self):
        assert 'a' == self.board.index_to_col(0)
        assert 'e' == self.board.index_to_col(4)
        assert 'h' == self.board.index_to_col(7)

    def test_translate_index_to_col_fail(self):
        with self.assertRaises(IncorrectIndex):
            self.board.index_to_col('1')

        with self.assertRaises(IncorrectIndex):
            self.board.index_to_col('z')

    def test_translate_index_to_row_pass(self):
        assert '8' == self.board.index_to_row(0)
        assert '3' == self.board.index_to_row(5)
        assert '1' == self.board.index_to_row(7)

    def test_translate_index_to_row_fail(self):
        with self.assertRaises(IncorrectIndex):
            self.board.index_to_row(-1)

        with self.assertRaises(IncorrectIndex):
            self.board.index_to_row(8)

        with self.assertRaises(IncorrectIndex):
            self.board.index_to_row('z')

    def test_index_to_notation_pass(self):
        assert 'a8' == self.board.index_to_notation((0, 0))
        assert 'h8' == self.board.index_to_notation((7, 0))
        assert 'a1' == self.board.index_to_notation((0, 7))
        assert 'h1' == self.board.index_to_notation((7, 7))

    def test_index_to_notation_fail(self):
        with self.assertRaises(IncorrectIndex):
            self.board.index_to_notation('dupa')

        with self.assertRaises(IncorrectIndex):
            self.board.index_to_notation(['a', 7])

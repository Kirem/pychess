# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans import King, Rook
from pychess.board import ChessBoard
from unittest import TestCase


class TestKing(TestCase):
    def setUp(self):
        self.king = King('a1', 'white')
        self.board = ChessBoard()

    def test_get_all_legal_moves(self):
        self.king.position = 'd3'
        self.board.add_chessman(self.king)
        legal_moves = self.king.get_all_legal_moves(self.board)
        assert ['c2', 'c3', 'c4', 'd2', 'd4', 'e2', 'e3', 'e4'] == sorted(legal_moves)

    def test_get_all_legal_moves_in_corner(self):
        self.board.add_chessman(self.king)
        legal_moves = self.king.get_all_legal_moves(self.board)
        assert ['a2', 'b1', 'b2'] == sorted(legal_moves)

    def test_get_all_legal_moves_with_enemy_chessman(self):
        self.board.add_chessman(self.king)
        self.board.add_chessman(Rook('a2', 'black'))
        self.board.add_chessman(Rook('b2', 'black'))
        legal_moves = self.king.get_all_legal_moves(self.board)
        assert ['a2', 'b1', 'b2'] == sorted(legal_moves)

    def test_get_all_legal_moves_with_own_chessman(self):
        self.board.add_chessman(self.king)
        self.board.add_chessman(Rook('b1', 'white'))
        legal_moves = self.king.get_all_legal_moves(self.board)
        assert ['a2', 'b2'] == sorted(legal_moves)

        self.board.add_chessman(Rook('b2', 'white'))
        legal_moves = self.king.get_all_legal_moves(self.board)
        assert ['a2'] == sorted(legal_moves)

        self.board.add_chessman(Rook('a2', 'white'))
        legal_moves = self.king.get_all_legal_moves(self.board)
        assert [] == sorted(legal_moves)

    def test_is_legal_move(self):
        self.board.add_chessman(self.king)
        self.board.add_chessman(Rook('a2', 'black'))
        self.board.add_chessman(Rook('b2', 'black'))
        assert self.king.is_legal_move(self.board, 'a2') is True
        assert self.king.is_legal_move(self.board, 'a3') is False

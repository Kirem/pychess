# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans import Knight, King
from pychess.board import ChessBoard
from unittest import TestCase


class TestKnightMoves(TestCase):
    def setUp(self):
        self.knight = Knight('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.knight)

    def test_get_all_legal_moves(self):
        c, r = self.knight.notation_to_index()
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['b3', 'b5', 'f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_enemy(self):
        self.board.add_chessman(King('b5', 'black'))
        c, r = self.knight.notation_to_index()
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['b3', 'b5', 'f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('b3', 'black'))
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['b3', 'b5', 'f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('b2', 'black'))
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['b3', 'b5', 'f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_own(self):
        self.board.add_chessman(King('b2', 'white'))
        c, r = self.knight.notation_to_index()
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['b3', 'b5', 'f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('b5', 'white'))
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['b3', 'f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('b3', 'white'))
        legal_moves = self.knight.get_all_legal_moves(self.board)
        expected_moves = ['f3', 'f5', 'c2', 'c6', 'e2', 'e6']
        assert sorted(expected_moves) == sorted(legal_moves)

# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans_moves import HorizontalMoves, VerticalMoves, DiagonalMoves
from pychess.board import ChessBoard
from pychess.chessmans import King
from unittest import TestCase


class TestHorizontalMoves(TestCase):
    def setUp(self):
        class TestChessman(HorizontalMoves, King):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

        self.horizontal = TestChessman('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.horizontal)

    def test_get_horizontal_moves(self):
        c, r = self.horizontal.notation_to_index()
        legal_moves = self.horizontal.get_horizontal_moves(self.board, c, r)
        assert ['a4', 'b4', 'c4', 'e4', 'f4', 'g4', 'h4'] == sorted(legal_moves)

    def test_get_horizontal_moves_with_enemy(self):
        self.board.add_chessman(King('c4', 'black'))
        c, r = self.horizontal.notation_to_index()
        legal_moves = self.horizontal.get_horizontal_moves(self.board, c, r)
        assert ['c4', 'e4', 'f4', 'g4', 'h4'] == sorted(legal_moves)

        self.board.add_chessman(King('e4', 'black'))
        legal_moves = self.horizontal.get_horizontal_moves(self.board, c, r)
        assert ['c4', 'e4'] == sorted(legal_moves)

    def test_get_horizontal_moves_with_own(self):
        self.board.add_chessman(King('c4', 'white'))
        c, r = self.horizontal.notation_to_index()
        legal_moves = self.horizontal.get_horizontal_moves(self.board, c, r)
        assert ['e4', 'f4', 'g4', 'h4'] == sorted(legal_moves)

        self.board.add_chessman(King('e4', 'white'))
        legal_moves = self.horizontal.get_horizontal_moves(self.board, c, r)
        assert [] == sorted(legal_moves)


class TestVerticalMoves(TestCase):
    def setUp(self):
        class TestChessman(VerticalMoves, King):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

        self.vertical = TestChessman('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.vertical)

    def test_get_vertical_moves(self):
        c, r = self.vertical.notation_to_index()
        legal_moves = self.vertical.get_vertical_moves(self.board, c, r)
        assert ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8'] == sorted(legal_moves)

    def test_get_vertical_moves_with_enemy(self):
        self.board.add_chessman(King('d3', 'black'))
        c, r = self.vertical.notation_to_index()
        legal_moves = self.vertical.get_vertical_moves(self.board, c, r)
        assert ['d3', 'd5', 'd6', 'd7', 'd8'] == sorted(legal_moves)

        self.board.add_chessman(King('d5', 'black'))
        legal_moves = self.vertical.get_vertical_moves(self.board, c, r)
        assert ['d3', 'd5'] == sorted(legal_moves)

    def test_get_vertical_moves_with_own(self):
        self.board.add_chessman(King('d3', 'white'))
        c, r = self.vertical.notation_to_index()
        legal_moves = self.vertical.get_vertical_moves(self.board, c, r)
        assert ['d5', 'd6', 'd7', 'd8'] == sorted(legal_moves)

        self.board.add_chessman(King('d5', 'white'))
        legal_moves = self.vertical.get_vertical_moves(self.board, c, r)
        assert [] == sorted(legal_moves)


class TestDiagonalMoves(TestCase):
    def setUp(self):
        class TestChessman(DiagonalMoves, King):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

        self.diagonal = TestChessman('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.diagonal)

    def test_get_diagonal_moves(self):
        c, r = self.diagonal.notation_to_index()
        legal_moves = self.diagonal.get_diagonal_moves(self.board, c, r)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_diagonal_moves_with_enemy(self):
        self.board.add_chessman(King('c3', 'black'))
        c, r = self.diagonal.notation_to_index()
        legal_moves = self.diagonal.get_diagonal_moves(self.board, c, r)
        expected_moves = ['c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e3', 'black'))
        legal_moves = self.diagonal.get_diagonal_moves(self.board, c, r)
        expected_moves = ['c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_diagonal_moves_with_own(self):
        self.board.add_chessman(King('c3', 'white'))
        c, r = self.diagonal.notation_to_index()
        legal_moves = self.diagonal.get_diagonal_moves(self.board, c, r)
        expected_moves = ['e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e3', 'white'))
        legal_moves = self.diagonal.get_diagonal_moves(self.board, c, r)
        expected_moves = ['e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5']
        assert sorted(expected_moves) == sorted(legal_moves)

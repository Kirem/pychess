# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans import Queen, King
from pychess.board import ChessBoard
from unittest import TestCase


class TestQueenMoves(TestCase):
    def setUp(self):
        self.queen = Queen('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.queen)

    def test_get_all_legal_moves(self):
        c, r = self.queen.notation_to_index()
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        expected_moves += ['a4', 'b4', 'c4', 'e4', 'f4', 'g4', 'h4']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_enemy(self):
        self.board.add_chessman(King('c4', 'black'))
        c, r = self.queen.notation_to_index()
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        expected_moves += ['c4', 'e4', 'f4', 'g4', 'h4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e4', 'black'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d3', 'black'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['d3', 'd5', 'd6', 'd7', 'd8'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d5', 'black'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['d3', 'd5'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('c3', 'black'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['d3', 'd5'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e3', 'black'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3']
        expected_moves += ['d3', 'd5'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_own(self):
        self.board.add_chessman(King('c4', 'white'))
        c, r = self.queen.notation_to_index()
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += ['e4', 'f4', 'g4', 'h4'] + ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e4', 'white'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += [] + ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d3', 'white'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += [] + ['d5', 'd6', 'd7', 'd8']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d5', 'white'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += [] + []
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('c3', 'white'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        expected_moves += [] + []
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e3', 'white'))
        legal_moves = self.queen.get_all_legal_moves(self.board)
        expected_moves = ['e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5']
        expected_moves += [] + []
        assert sorted(expected_moves) == sorted(legal_moves)

# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans import Bishop, King
from pychess.board import ChessBoard
from unittest import TestCase


class TestBishopMoves(TestCase):
    def setUp(self):
        self.bishop = Bishop('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.bishop)

    def test_get_diagonal_moves(self):
        c, r = self.bishop.notation_to_index()
        legal_moves = self.bishop.get_all_legal_moves(self.board)
        expected_moves = ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_enemy(self):
        self.board.add_chessman(King('c3', 'black'))
        c, r = self.bishop.notation_to_index()
        legal_moves = self.bishop.get_all_legal_moves(self.board)
        expected_moves = ['c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e3', 'black'))
        legal_moves = self.bishop.get_all_legal_moves(self.board)
        expected_moves = ['c3', 'e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_own(self):
        self.board.add_chessman(King('c3', 'white'))
        c, r = self.bishop.notation_to_index()
        legal_moves = self.bishop.get_all_legal_moves(self.board)
        expected_moves = ['e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5', 'e3', 'f2', 'g1']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e3', 'white'))
        legal_moves = self.bishop.get_all_legal_moves(self.board)
        expected_moves = ['e5', 'f6', 'g7', 'h8',
                          'a7', 'b6', 'c5']
        assert sorted(expected_moves) == sorted(legal_moves)

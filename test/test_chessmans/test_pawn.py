# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans import Pawn, Rook
from pychess.board import ChessBoard
from unittest import TestCase


class TestPawn(TestCase):

    def setUp(self):
        self.pawn = Pawn('a2', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.pawn)

    def test_get_all_legal_moves(self):
        legal_moves = self.pawn.get_all_legal_moves(self.board)
        assert ['a3', 'a4'] == sorted(legal_moves)

    def test_get_all_legal_moves_first_short_move(self):
        assert self.board.move('a2', 'a3') is True
        legal_moves = self.pawn.get_all_legal_moves(self.board)
        assert ['a4'] == sorted(legal_moves)

    def test_get_all_legal_moves_first_long_move(self):
        assert self.board.move('a2', 'a4') is True
        legal_moves = self.pawn.get_all_legal_moves(self.board)
        assert ['a5'] == sorted(legal_moves)

    def test_get_all_legal_moves_with_enemy_chessman(self):
        self.board.add_chessman(Rook('a3', 'black'))
        self.board.add_chessman(Rook('b3', 'black'))
        legal_moves = self.pawn.get_all_legal_moves(self.board)
        assert ['b3'] == sorted(legal_moves)

    def test_get_all_legal_moves_with_own_chessmans(self):
        self.board.add_chessman(Rook('b3', 'white'))
        legal_moves = self.pawn.get_all_legal_moves(self.board)
        assert ['a3', 'a4'] == sorted(legal_moves)

        self.board.add_chessman(Rook('a3', 'white'))
        legal_moves = self.pawn.get_all_legal_moves(self.board)
        assert [] == sorted(legal_moves)

# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.chessmans import Rook, King
from pychess.board import ChessBoard
from unittest import TestCase


class TestRookMoves(TestCase):
    def setUp(self):
        self.rook = Rook('d4', 'white')
        self.board = ChessBoard()
        self.board.add_chessman(self.rook)

    def test_get_all_legal_moves(self):
        c, r = self.rook.notation_to_index()
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        expected_moves += ['a4', 'b4', 'c4', 'e4', 'f4', 'g4', 'h4']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_enemy(self):
        self.board.add_chessman(King('c4', 'black'))
        c, r = self.rook.notation_to_index()
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        expected_moves += ['c4', 'e4', 'f4', 'g4', 'h4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e4', 'black'))
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d3', 'black'))
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = ['d3', 'd5', 'd6', 'd7', 'd8'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d5', 'black'))
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = ['d3', 'd5'] + ['c4', 'e4']
        assert sorted(expected_moves) == sorted(legal_moves)

    def test_get_all_legal_moves_with_own(self):
        self.board.add_chessman(King('c4', 'white'))
        c, r = self.rook.notation_to_index()
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = ['e4', 'f4', 'g4', 'h4'] + ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('e4', 'white'))
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = [] + ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d3', 'white'))
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = [] + ['d5', 'd6', 'd7', 'd8']
        assert sorted(expected_moves) == sorted(legal_moves)

        self.board.add_chessman(King('d5', 'white'))
        legal_moves = self.rook.get_all_legal_moves(self.board)
        expected_moves = [] + []
        assert sorted(expected_moves) == sorted(legal_moves)

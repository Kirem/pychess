# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from pychess.exception import ColorNotRecogized
from pychess.chessmans import King
from unittest import TestCase


class TestColors(TestCase):
    def setUp(self):
        self.king = King('a1', 'white')

    def test_check_color_pass(self):
        self.king.check_color('white')
        self.king.check_color('black')

    def test_check_color_failed(self):
        with self.assertRaises(ColorNotRecogized):
            self.king.check_color('blue')

        with self.assertRaises(ColorNotRecogized):
            self.king.check_color('red')

    def test_is_white(self):
        assert self.king.is_white() is True
        new_king = King('b1', 'black')
        assert new_king.is_white() is False


class TestChessman(TestCase):
    def setUp(self):
        self.king = King('a1', 'white')

    def test_reduce(self):
        assert self.king.reduce([-1, 0, 1, 3, 7, 8, 100]) == [0, 1, 3, 7]

    def test_get_permutations(self):
        permutations = sorted(self.king.get_permutations([1, 2], [3, 4]))
        assert permutations == [(1, 3), (1, 4), (2, 3), (2, 4)]

        permutations = sorted(self.king.get_permutations([4, 5], [1, 2, 3]))
        assert permutations == [(4, 1), (4, 2), (4, 3), (5, 1), (5, 2), (5, 3)]

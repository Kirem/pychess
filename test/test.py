from unittest import TestCase

from pychass.listeners import GameStateListener


class Test(TestCase):
    def test(self):
        listeners = GameStateListener()
        with self.assertRaises(NotImplementedError):
            listeners.on_game_started(1)

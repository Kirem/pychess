# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
from pychess.exception import CanNotAddChessman, CanNotRemoveChessman, CanNotCaptureChessman
from pychess.chessmans import King, Queen, Rook, Knight, Bishop, Pawn
from pychess.board import ChessBoard
from unittest import TestCase
from mock import Mock


class TestChessBoard(TestCase):
    def setUp(self):
        self.board = ChessBoard()
        self.board.initialize()

    def test_add_chessman_pass(self):
        assert True is self.board.is_empty('d3')
        self.board.add_chessman(Rook('d3', 'white'))
        assert False is self.board.is_empty('d3')

    def test_add_chessman_fail(self):
        self.board.add_chessman(Rook('d3', 'white'))
        assert False is self.board.is_empty('d3')
        with self.assertRaises(CanNotAddChessman):
            self.board.add_chessman(Rook('d3', 'black'))

    def test_remove_chessman_pass(self):
        rook = Rook('d3', 'white')
        assert True is self.board.is_empty('d3')
        self.board.add_chessman(rook)
        assert False is self.board.is_empty('d3')
        self.board.remove_chessman(rook)
        assert True is self.board.is_empty('d3')

    def test_remove_chessman_fail_not_active(self):
        rook = Rook('d3', 'white')
        rook.active = False
        with self.assertRaises(CanNotRemoveChessman):
            self.board.remove_chessman(rook)

    def test_remove_chessman_fail_field_is_empty(self):
        assert True is self.board.is_empty('d3')
        with self.assertRaises(CanNotRemoveChessman):
            self.board.remove_chessman(Rook('d3', 'black'))

    def test_remove_chessman_fail_field_not_the_same_chessmans(self):
        self.board.add_chessman(Rook('d3', 'white'))
        with self.assertRaises(CanNotRemoveChessman):
            self.board.remove_chessman(Rook('d3', 'black'))

    def test_initialize_board_black(self):
        assert all([ch_m.color == 'black' for ch_m in self.board.board[0]])

        assert type(self.board.board[0][0]) is Rook
        assert self.board.board[0][0].position == 'a8'

        assert type(self.board.board[0][1]) is Knight
        assert self.board.board[0][1].position == 'b8'

        assert type(self.board.board[0][2]) is Bishop
        assert self.board.board[0][2].position == 'c8'

        assert type(self.board.board[0][3]) is Queen
        assert self.board.board[0][3].position == 'd8'

        assert type(self.board.board[0][4]) is King
        assert self.board.board[0][4].position == 'e8'

        assert type(self.board.board[0][5]) is Bishop
        assert self.board.board[0][5].position == 'f8'

        assert type(self.board.board[0][6]) is Knight
        assert self.board.board[0][6].position == 'g8'

        assert type(self.board.board[0][7]) is Rook
        assert self.board.board[0][7].position == 'h8'

        assert all([ch_m.color == 'black' for ch_m in self.board.board[1]])
        assert all([type(ch_m) is Pawn for ch_m in self.board.board[1]])

    def test_initialize_board_white(self):
        assert all([ch_m.color == 'white' for ch_m in self.board.board[7]])

        assert type(self.board.board[7][0]) is Rook
        assert self.board.board[7][0].position == 'a1'

        assert type(self.board.board[7][1]) is Knight
        assert self.board.board[7][1].position == 'b1'

        assert type(self.board.board[7][2]) is Bishop
        assert self.board.board[7][2].position == 'c1'

        assert type(self.board.board[7][3]) is Queen
        assert self.board.board[7][3].position == 'd1'

        assert type(self.board.board[7][4]) is King
        assert self.board.board[7][4].position == 'e1'

        assert type(self.board.board[7][5]) is Bishop
        assert self.board.board[7][5].position == 'f1'

        assert type(self.board.board[7][6]) is Knight
        assert self.board.board[7][6].position == 'g1'

        assert type(self.board.board[7][7]) is Rook
        assert self.board.board[7][7].position == 'h1'

        assert all([ch_m.color == 'white' for ch_m in self.board.board[6]])
        assert all([type(ch_m) is Pawn for ch_m in self.board.board[6]])

    def test_initialize_board_empty_places(self):
        assert all([place is None for place in self.board.board[2]])
        assert all([place is None for place in self.board.board[3]])
        assert all([place is None for place in self.board.board[4]])
        assert all([place is None for place in self.board.board[5]])

    def test_capture_chessman_pass(self):
        rook = Rook('d3', 'white')
        self.board.add_chessman(rook)
        assert False is self.board.is_empty('d3')
        self.board.capture_chessman(rook)
        assert True is self.board.is_empty('d3')
        assert self.board.captured['white'][0] is rook

    def test_capture_chessman_failed(self):
        rook = Rook('d3', 'white')
        rook.active = False
        with self.assertRaises(CanNotCaptureChessman):
            self.board.capture_chessman(rook)

    def test_get_chessman(self):
        chessman = self.board.get_chessman('a1')
        assert type(chessman) is Rook
        assert chessman.position == 'a1'

        chessman = self.board.get_chessman('d3')
        assert chessman is None

    def test_is_empty(self):
        assert self.board.is_empty('a1') is False
        assert self.board.is_empty('h8') is False
        assert self.board.is_empty('d3') is True
        assert self.board.is_empty('e6') is True

    def test_move_chessman(self):
        rook = Rook('d3', 'white')
        self.board.add_chessman(rook)
        assert False is self.board.is_empty('d3')
        assert True is self.board.is_empty('d4')

        self.board.move_chessman(rook, 'd4')
        assert True is self.board.is_empty('d3')
        assert False is self.board.is_empty('d4')

        assert rook is self.board.get_chessman('d4')

    def test_move_true(self):
        rook = Rook('d3', 'white')
        rook.is_legal_move = Mock()
        rook.is_legal_move.return_value = True
        self.board.add_chessman(rook)
        assert self.board.get_chessman('d3') is rook
        assert self.board.get_chessman('d4') is None

        assert self.board.move('d3', 'd4') is True
        assert self.board.get_chessman('d4') is rook
        assert self.board.get_chessman('d3') is None

    def test_move_false(self):
        rook = Rook('d3', 'white')
        rook.is_legal_move = Mock()
        rook.is_legal_move.return_value = False
        self.board.add_chessman(rook)
        assert self.board.get_chessman('d3') is rook
        assert self.board.get_chessman('d4') is None

        assert self.board.move('d3', 'd4') is False
        assert self.board.get_chessman('d3') is rook
        assert self.board.get_chessman('d4') is None

    def test_move_with_capture(self):
        rook = Rook('d3', 'white')
        rook.is_legal_move = Mock()
        rook.is_legal_move.return_value = True
        pawn = Pawn('d4', 'black')
        self.board.add_chessman(rook)
        self.board.add_chessman(pawn)
        assert self.board.get_chessman('d3') is rook
        assert self.board.get_chessman('d4') is pawn

        assert self.board.move('d3', 'd4') is True
        assert self.board.get_chessman('d4') is rook
        assert self.board.get_chessman('d3') is None
        assert self.board.captured['black'][0] is pawn

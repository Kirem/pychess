# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""


class Moves:
    pass


class PrimitiveMoves(Moves):

    def _get_all_legal_moves(self, zips, board, c, r):
        legal_moves = []
        for _zip in zips:
            for col, row in _zip:
                field = self.index_to_notation((col, row))
                if board.is_empty(field):
                    legal_moves.append(field)
                elif board.get_chessman(field).color != self.color:
                    legal_moves.append(field)
                    break
                else:
                    break

        return legal_moves


class DiagonalMoves(PrimitiveMoves):
    def get_diagonal_zips(self, c, r):
        zips = [zip(range(c + 1, self.MAX_INDEX + 1, 1), range(r + 1, self.MAX_INDEX + 1, 1)),
                zip(range(c - 1, self.MIN_INDEX - 1, -1), range(r + 1, self.MAX_INDEX + 1, 1)),
                zip(range(c - 1, self.MIN_INDEX - 1, -1), range(r - 1, self.MIN_INDEX - 1, -1)),
                zip(range(c + 1, self.MAX_INDEX + 1, 1), range(r - 1, self.MIN_INDEX - 1, -1))]
        return zips

    def get_diagonal_moves(self, board, c, r):
        return self._get_all_legal_moves(self.get_diagonal_zips(c, r), board, c, r)


class VerticalMoves(PrimitiveMoves):
    def get_vertical_zips(self, c, r):
        up = range(r + 1, self.MAX_INDEX + 1, 1)
        down = range(r - 1, self.MIN_INDEX - 1, -1)
        zips = [zip([c] * len(up), up), zip([c] * len(down), down)]
        return zips

    def get_vertical_moves(self, board, c, r):
        return self._get_all_legal_moves(self.get_vertical_zips(c, r), board, c, r)


class HorizontalMoves(PrimitiveMoves):
    def get_horizontal_zips(self, c, r):
        left = range(c - 1, self.MIN_INDEX - 1, -1)
        right = range(c + 1, self.MAX_INDEX + 1, 1)
        zips = [zip(left, [r] * len(left)), zip(right, [r] * len(right))]
        return zips

    def get_horizontal_moves(self, board, c, r):
        return self._get_all_legal_moves(self.get_horizontal_zips(c, r), board, c, r)


class QueenMoves(VerticalMoves, HorizontalMoves, DiagonalMoves):
    pass


class RookMoves(VerticalMoves, HorizontalMoves):
    pass


class BishopMoves(DiagonalMoves):
    pass


class PawnMoves(Moves):
    def get_diagonal_moves(self, board, c, r):
        legal_moves = []
        for index in self.get_permutations([c - self.direction, c + self.direction],
                                           [r + self.direction]):
            move = self.index_to_notation(index)
            if not board.is_empty(move) and board.get_chessman(move).color != self.color:
                legal_moves.append(move)
        return legal_moves

    def get_forward_moves(self, board, c, r):
        legal_moves = []
        one_field = [c, r + self.direction]
        if len(self.reduce(one_field)) == 2:
            move = self.index_to_notation(one_field)
            if board.is_empty(move):
                legal_moves.append(move)
        if self.possible_long_move and len(legal_moves) == 1:
            move = self.index_to_notation([c, r + (2 * self.direction)])
            if board.is_empty(move):
                legal_moves.append(move)

        return legal_moves


class KingMoves(Moves):
    def get_around_moves(self, board, c, r):
        legal_moves = []
        for index in self.get_permutations([c - 1, c, c + 1], [r - 1, r, r + 1]):
            move = self.index_to_notation(index)
            if board.is_empty(move) or board.get_chessman(move).color != self.color:
                legal_moves.append(move)
        return legal_moves


class KnightMoves(Moves):
    def get_all_moves(self, board, c, r):
        legal_moves = []
        vertical = self.get_permutations([c - 1, c + 1], [r - 2, r + 2])
        horizontal = self.get_permutations([c - 2, c + 2], [r - 1, r + 1])

        for index in list(vertical) + list(horizontal):
            move = self.index_to_notation(index)
            if board.is_empty(move) or board.get_chessman(move).color != self.color:
                legal_moves.append(move)
        return legal_moves

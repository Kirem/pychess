# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
from abc import ABCMeta
from itertools import product

from colorama import Fore

from pychess.base import BaseOperations
from pychess.chessmans_moves import (BishopMoves, KingMoves, KnightMoves,
                                     PawnMoves, QueenMoves, RookMoves)
from pychess.exception import ColorNotRecogized


class ChessmanColor(metaclass=ABCMeta):
    WHITE = 'white'
    BLACK = 'black'

    def __init__(self, position, color):
        self._position = position
        self.check_color(color)
        self.color = color
        self.printing_color = Fore.WHITE if color == self.WHITE else Fore.BLACK
        self.active = True

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, val):
        self._position = val

    def check_color(self, color):
        if color not in [self.WHITE, self.BLACK]:
            raise ColorNotRecogized('Can not recognize color. Color should be white or black.')

    def is_white(self):
        return self.color == self.WHITE

    def __str__(self):
        return self.printing_color + "{:^6}".format(self.base_str()) + Fore.WHITE

    def is_legal_move(self, board, new_position):
        return new_position in self.get_all_legal_moves(board)

    def position_on_board(self):
        pos = list(self.position)
        return ("abcdefgh".index(pos[0]), int(pos[1]) - 1)


class Chessman(ChessmanColor, BaseOperations):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def notation_to_index(self):
        return super().notation_to_index(self.position)

    def reduce(self, l):
        return [el for el in l if self.MIN_INDEX <= el <= self.MAX_INDEX]

    def get_permutations(self, l1, l2):
        return product(self.reduce(l1), self.reduce(l2))


class King(Chessman, KingMoves):
    def get_all_legal_moves(self, board):
        c, r = self.notation_to_index()
        return self.get_around_moves(board, c, r)

    def base_str(self):
        return 'King'


class Queen(Chessman, QueenMoves):
    def get_all_legal_moves(self, board):
        c, r = self.notation_to_index()
        legal_moves = self.get_horizontal_moves(board, c, r)
        legal_moves += self.get_vertical_moves(board, c, r)
        legal_moves += self.get_diagonal_moves(board, c, r)
        return legal_moves

    def base_str(self):
        return 'Queen'


class Rook(Chessman, RookMoves):
    def get_all_legal_moves(self, board):
        c, r = self.notation_to_index()
        legal_moves = self.get_horizontal_moves(board, c, r)
        legal_moves += self.get_vertical_moves(board, c, r)
        return legal_moves

    def base_str(self):
        return 'Rook'


class Bishop(Chessman, BishopMoves):
    def get_all_legal_moves(self, board):
        c, r = self.notation_to_index()
        return self.get_diagonal_moves(board, c, r)

    def base_str(self):
        return 'Bishop'


class Knight(Chessman, KnightMoves):
    def get_all_legal_moves(self, board):
        c, r = self.notation_to_index()
        return self.get_all_moves(board, c, r)

    def base_str(self):
        return 'Knight'


class Pawn(Chessman, PawnMoves):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.possible_long_move = True
        self.direction = -1 if self.is_white() else 1

    def get_all_legal_moves(self, board):
        c, r = self.notation_to_index()
        legal_moves = []
        legal_moves += self.get_diagonal_moves(board, c, r)
        legal_moves += self.get_forward_moves(board, c, r)
        return legal_moves

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, val):
        _, old_r = self.notation_to_index()
        self._position = val
        if old_r in [1, 6]:
            self.possible_long_move = False

    def base_str(self):
        return 'Pawn'

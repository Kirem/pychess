# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""


class IncorrectPosition(Exception):
    pass


class IncorrectRow(Exception):
    pass


class IncorrectColumn(Exception):
    pass


class IncorrectIndex(Exception):
    pass


class ColorNotRecogized(Exception):
    pass


class CanNotAddChessman(Exception):
    pass


class CanNotRemoveChessman(Exception):
    pass


class CanNotCaptureChessman(Exception):
    pass


class IncorrectColorMoveException(Exception):
    pass

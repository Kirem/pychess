from pkg_resources import resource_filename
import pygame


class Sprite(object):

    def __init__(self, file, size):
        self.file = file
        self.image = pygame.image.load(self.file)
        scale = float(self.image.get_width()) / float(self.image.get_height())
        self.height = int(size)
        self.width = int(size * scale)
        self.image = pygame.transform.scale(
            self.image, (self.width, self.height))

    def image_path(self, image_name):
        return resource_filename('pychess', "res/{}".format(image_name))


class BoardSprite(Sprite):

    def __init__(self):
        super().__init__(self.image_path("board.png"), 480)
        self.size = 480
        self.tile_size = self.size / 8
        self.position = (0, 0)

    def render(self, surface, position):
        surface.blit(self.image, position)
        self.position = position

    def draw_figure(self, surface, figure, tile_pos):
        x_on_tile = (self.tile_size - figure.width) / 2
        y_on_tile = (self.tile_size - figure.height) / 2
        tile_x_pos = self.position[0] + (tile_pos[0]) * self.tile_size
        tile_y_pos = self.position[1] + (7 - tile_pos[1]) * self.tile_size
        surface.blit(figure.image, (tile_x_pos +
                                    x_on_tile, tile_y_pos + y_on_tile))

    def resolve_event_position(self, event_pos):
        if (event_pos[0] < 0 or event_pos[0] > self.size or
                event_pos[1] < 0 or event_pos[1] > self.size):
            return (-1, -1)
        return (int(event_pos[0] / (self.tile_size)),
                int(event_pos[1] / (self.tile_size)))


class FigureSprite(Sprite):
    def __init__(self, file):
        super().__init__(file, (480 / 16) + 10)

    def _color_surface(self, surface, colors):
        arr = pygame.surfarray.pixels3d(surface)
        arr[:, :, 0] = colors[0]
        arr[:, :, 1] = colors[1]
        arr[:, :, 2] = colors[2]

    def color(self, is_white):
        self.image.convert_alpha()
        self._color_surface(self.image, (200, 200, 200)
                            if is_white else (0, 0, 0))


class RookSprite(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("rook.png"))


class KingSprit(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("king.png"))


class PawnSprite(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("pawn.png"))


class QueenSprite(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("queen.png"))


class KingSprite(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("king.png"))


class BishopSprite(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("bishop.png"))


class KnightSprite(FigureSprite):
    def __init__(self):
        super().__init__(self.image_path("knight.png"))

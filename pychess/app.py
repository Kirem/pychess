import sys
import pygame

from .model import ChessModel
from .presenter import ChessPresenter
from .view import ChessView


class App(object):
    def __init__(self):
        pygame.init()
        pygame.display.set_caption('Chess')

        self.start()

    def start(self):
        view = ChessView(ChessPresenter(ChessModel()), self)
        view.new_game()

    def quit_game(self):
        pygame.quit()
        sys.exit()


if __name__ == '__main__':
    APP = App()

# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
from .exception import (CanNotAddChessman, CanNotRemoveChessman,
                        CanNotCaptureChessman, IncorrectColorMoveException)
from .exception import IncorrectPosition
from .chessmans import King, Queen, Rook, Knight, Bishop, Pawn
from .base import BaseOperations


class ChessBoard(BaseOperations):

    def __init__(self):
        super().__init__()
        self.board = [[None] * 8 for _ in range(8)]
        self.captured = {'white': [], 'black': []}

    def __str__(self):
        cols = "    " + "      ".join('abcdefgh') + "\n"
        board = "" + cols
        row_num = 8
        for row in self.board:
            board += "{}".format(row_num)
            for col in row:
                board += " {}".format(str(col)
                                      if col is not None else " None ")
            board += " {}\n".format(row_num)
            row_num -= 1
        board += cols
        return board

    def initialize(self):
        order = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]
        for num, color in (('8', 'black'), ('1', 'white')):
            for Chessman, column in zip(order, 'abcdefgh'):
                chessman = Chessman(column + num, color)
                self.add_chessman(chessman)

        for num, color in (('7', 'black'), ('2', 'white')):
            for column in 'abcdefgh':
                pawn = Pawn(column + num, color)
                self.add_chessman(pawn)

    def add_chessman(self, chessman):
        if not self.is_empty(chessman.position):
            raise CanNotAddChessman(
                'Field: {} is not empty.'.format(chessman.position))
        c, r = self.translate_position(chessman.position)
        self.board[r][c] = chessman

    def remove_chessman(self, chessman):
        if not chessman.active:
            raise CanNotRemoveChessman(
                'Can not remove {} one more time'.format(chessman))
        if chessman is not self.get_chessman(chessman.position):
            raise CanNotRemoveChessman('Can not remove {}. On filed {} is diffrent chessman'.format(
                chessman,
                chessman.position))

        c, r = self.translate_position(chessman.position)
        self.board[r][c] = None

    def capture_chessman(self, chessman):
        if not chessman.active:
            raise CanNotCaptureChessman('{} is not active'.format(chessman))
        self.remove_chessman(chessman)
        self.captured[chessman.color].append(chessman)

    def get_chessman(self, position):
        c, r = self.translate_position(position)
        return self.board[r][c]

    def is_empty(self, position):
        c, r = self.translate_position(position)
        return self.board[r][c] is None

    def move_chessman(self, chessman, new_position):
        self.remove_chessman(chessman)
        chessman.position = new_position
        self.add_chessman(chessman)

    def move(self, current_position, new_position, color_moving='white'):
        print(current_position)
        print(new_position)
        chessman = self.get_chessman(current_position)
        if chessman is None:
            raise IncorrectPosition('Can not make move.'
                                    'On starting position do not stay any chessman')

        if chessman.color != color_moving:
            raise IncorrectColorMoveException('Next move is ' + color_moving)

        if chessman.is_legal_move(self, new_position):
            if not self.is_empty(new_position):
                self.capture_chessman(self.get_chessman(new_position))
            self.move_chessman(chessman, new_position)
            return True
        else:
            return False

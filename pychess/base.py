# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek.karpiewski@holdapp.com
"""
from .exception import IncorrectColumn, IncorrectRow, IncorrectPosition, IncorrectIndex


class BaseOperations:
    MIN_INDEX = 0
    MAX_INDEX = 7
    rows_notation_to_index = {l: inx for inx, l in enumerate('87654321')}
    cols_notation_to_index = {n: inx for inx, n in enumerate('abcdefgh')}
    rows_index_to_notation = {inx: l for l, inx in rows_notation_to_index.items()}
    cols_index_to_notation = {inx: l for l, inx in cols_notation_to_index.items()}

    def notation_to_index(self, position):
        if len(position) != 2 or type(position) is not str:
            raise IncorrectPosition('Incorect position: {}'.format(position))
        return (self.col_to_index(position[0]), self.row_to_index(position[1]))

    translate_position = notation_to_index  # for backward compability

    def index_to_notation(self, position):
        if len(position) != 2 or type(position[0]) is not int or type(position[0]) is not int:
            raise IncorrectIndex('Incorect position: {}'.format(position))
        return self.index_to_col(position[0]) + self.index_to_row(position[1])

    def col_to_index(self, col):
        if col not in self.cols_notation_to_index.keys():
            raise IncorrectColumn('Can not translate column: {}'.format(col))
        return self.cols_notation_to_index[col]

    def row_to_index(self, row):
        if row not in self.rows_notation_to_index.keys():
            raise IncorrectRow('Can not translate row: {}'.format(row))
        return self.rows_notation_to_index[row]

    def index_to_col(self, index):
        if index not in self.cols_index_to_notation.keys():
            raise IncorrectIndex('Can not translate index: {}'.format(index))
        return self.cols_index_to_notation[index]

    def index_to_row(self, index):
        if index not in self.rows_index_to_notation.keys():
            raise IncorrectIndex('Can not translate index: {}'.format(index))
        return self.rows_index_to_notation[index]

# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
from .listeners import GameStateListener


class ChessPresenter(GameStateListener):
    def __init__(self, chess_model):
        GameStateListener.__init__(self)
        self.chess_model = chess_model
        self.chess_view = None
        self.move_started = (0, 0)

    def new_game(self):
        self.chess_view.new_game()
        self.chess_model.new_game(self)

    def attach_view(self, view):
        self.chess_view = view

    def is_playing(self):
        return True

    def on_figure_clicked(self, position):
        print(position)
        self.move_started = position

    def on_figure_down(self, position):
        self.chess_model.move_ended(self.move_started, position)

    def on_game_started(self, game_number):
        raise NotImplementedError("You need to override this method")

    def on_turn_start(self, turn_num):
        raise NotImplementedError("You need to override this method")

    def on_game_finished(self, game_result):
        raise NotImplementedError("You need to override this method")

    def show_board(self, board):
        self.chess_view.show_board(board)
        print(board)

    def move_error(self, message):
        self.chess_view.show_error(message)

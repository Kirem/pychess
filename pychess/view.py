import pygame

from .board import ChessBoard
from .chessmans import (Bishop, King, Knight, Pawn, Queen, Rook)
from .sprites import (BishopSprite, BoardSprite, KingSprite, KnightSprite,
                      PawnSprite, QueenSprite, RookSprite)


class ChessView(object):
    sprite_map = {
        Rook: RookSprite(),
        King: KingSprite(),
        Queen: QueenSprite(),
        Pawn: PawnSprite(),
        Knight: KnightSprite(),
        Bishop: BishopSprite()
    }

    def __init__(self, presenter, app, width=640, height=640):
        self.app = app
        self.presenter = presenter
        self.width = width
        self.height = height
        self.turn = 1
        self.running = False
        self.surface = pygame.display.set_mode((width, height))
        self.surface.fill((255, 255, 255))
        self.font = pygame.font.SysFont("monospace", 15)
        self.board_sprite = BoardSprite()
        self.board = ChessBoard()
        self.board.initialize()
        self.pos = (0, 0)
        self.presenter.attach_view(self)
        self.presenter.new_game()

    def new_game(self):
        self.turn = 1
        self.pos = (self.height - 480) / 2
        self.board_sprite.render(self.surface, (self.pos, self.pos))

    def __event_loop(self):
        if self.running:
            return
        self.running = True
        while True:
            for event in pygame.event.get():
                self.__handle_event(event)
                pygame.display.update()

    def __handle_event(self, event):
        if event.type == pygame.QUIT:
            self.app.quit_game()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.__handle_mouse_down()
        elif event.type == pygame.MOUSEBUTTONUP:
            self.__handle_mouse_up()

    def __handle_mouse_down(self):
        event_pos = pygame.mouse.get_pos()
        pos_on_board = (event_pos[0] - self.pos, event_pos[1] - self.pos)
        clicked_tile = self.board_sprite.resolve_event_position(pos_on_board)
        self.presenter.on_figure_clicked(clicked_tile)

    def __handle_mouse_up(self):
        event_pos = pygame.mouse.get_pos()
        pos_on_board = (event_pos[0] - self.pos, event_pos[1] - self.pos)
        clicked_tile = self.board_sprite.resolve_event_position(pos_on_board)
        self.presenter.on_figure_down(clicked_tile)

    def show_board(self, board):
        self.board = board
        self.__clear_screen()
        self.__draw_board()

    def __draw_board(self):
        self.board_sprite.render(self.surface, (self.pos, self.pos))
        for row in self.board.board:
            for col in row:
                if col:
                    self.__draw_figure(col)
        self.__event_loop()

    def __clear_screen(self):
        pygame.draw.rect(self.surface, (255, 255, 255),
                         (0, 0, self.width, self.pos))
        pygame.display.update()

    def __draw_figure(self, col):
        figure = self.sprite_map.get(type(col), PawnSprite())
        figure.color(col.color == 'white')
        self.board_sprite.draw_figure(
            self.surface, figure, col.position_on_board())

    def show_error(self, message):
        self.__clear_screen()
        label = self.font.render(message, 1, (255, 0, 0))
        self.surface.blit(label, (0, 0))
        print(message)

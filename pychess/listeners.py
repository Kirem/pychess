# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""


class GameStateListener(object):
    """this class defines communication contract between model and presenter"""

    def __init__(self):
        pass

    def on_game_started(self, game_number):
        raise NotImplementedError("You need to override this method")

    def on_turn_start(self, turn_num):
        raise NotImplementedError("You need to override this method")

    def on_game_finished(self, game_result):
        raise NotImplementedError("You need to override this method")

    def show_board(self, board):
        raise NotImplementedError("You need to override this method")

    def move_error(self, message):
        raise NotImplementedError("You need to override this method")

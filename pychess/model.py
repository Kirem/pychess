# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
from .board import ChessBoard
from .chessmans import ChessmanColor
from .exception import IncorrectPosition, IncorrectColorMoveException


class ChessModel(object):
    def __init__(self):
        self.board = ChessBoard()
        self.board.initialize()
        self.game_state_listener = None
        self.turn = 1

    def move_ended(self, move_from, move_where):
        """move has ended at given position. function should return
        chess board state, or raise exception in case move is invalid"""
        try:
            color_moving = ChessmanColor.WHITE
            if self.turn % 2 == 0:
                color_moving = ChessmanColor.BLACK

            result = self.board.move(self.board.index_to_notation(move_from),
                                     self.board.index_to_notation(move_where),
                                     color_moving)
            if result:
                self.game_state_listener.show_board(self.board)
                self.turn += 1
            else:
                self.game_state_listener.move_error("Incorrect move")
        except IncorrectColorMoveException as exception:
            self.game_state_listener.move_error(str(exception))
        except IncorrectPosition as exception:
            self.game_state_listener.move_error(str(exception))

    def new_game(self, game_state_listener):
        """starts new game"""
        # todo
        self.game_state_listener = game_state_listener
        self.game_state_listener.show_board(self.board)

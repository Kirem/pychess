# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz, Miroslaw Karpiewski
:contact: jaroslaw.grycz@gmail.com, mirek0922@gmail.com
"""
import sys

import pygame

from .model import ChessModel
from .presenter import ChessPresenter


def main():
    presenters = ChessPresenter(ChessModel())
    print(presenters)
    pygame.init()
    displaysurface = pygame.display.set_mode((400, 300))
    pygame.display.set_caption('Hello World!')
    white = (255, 255, 255)
    # BLACK = (0, 0, 0)
    red = (255, 0, 0)
    green = (0, 255, 0)
    blue = (0, 0, 255)
    displaysurface.fill(white)
    pygame.draw.polygon(displaysurface, green, ((146, 0), (291, 106),
                                                (236, 277), (56, 277), (0, 106)))
    pygame.draw.line(displaysurface, blue, (60, 60), (120, 60), 4)
    pygame.draw.line(displaysurface, blue, (120, 60), (60, 120))
    pygame.draw.line(displaysurface, blue, (60, 120), (120, 120), 4)
    pygame.draw.circle(displaysurface, blue, (300, 50), 20, 0)
    pygame.draw.ellipse(displaysurface, red, (300, 250, 40, 80), 1)
    pygame.draw.rect(displaysurface, red, (200, 150, 100, 50))
    pygame.display.update()
    while True:  # main game loop
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                pygame.display.update()


if __name__ == '__main__':
    main()
